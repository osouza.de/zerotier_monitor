#!/bin/bash

# Usage:
# sh check.sh $(cat .secret)

# Requesting to zerotier network devices statuses.
request=$(curl -s https://my.zerotier.com/api/network/88503383902759a4/member \
     -H "Accept: application/json" \
     -H "Authorization: Bearer $1")

echo $request | jq -c -r '.[]'
 
items=$(echo "$request" | jq -c -r '.[]')
for item in ${items[@]}; do
    echo $item
    # whatever you are trying to do ...
done
